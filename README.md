# How To Run
1. Run the containers: `make run`
2. Create a non-empty file in `client/uploads`
3. Look at the root `uploads/` to verify that a new file was uploaded to the tusd server
4. Look at the terminal ouput to verify correct functioning