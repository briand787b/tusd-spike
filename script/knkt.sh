#!/bin/bash

service=$1

if [ -z $service ]; then
  echo "-n is a required flag"
  exit 1
fi

source .env

case $service in
  tus-client)
    docker-compose exec $service /bin/bash
    exit 0
    ;;
  tus-server)
    docker-compose exec $service /bin/sh
    exit 0
    ;;
esac
