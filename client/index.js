import Tus from 'tus-js-client';
import * as fs from 'fs';

const backendHost = process.env.BACKEND_HOST
console.log('backendHost: ', backendHost);

const fileNames = fs.readdirSync('./uploads');
console.log('fileNames: ', fileNames)

fileNames.map(name => handleFileUpload(`./uploads/${name}`));

function handleFileUpload(fileName) {
    try {
        const buffer = fs.readFileSync(fileName);
        console.log('buffer: ', buffer);

        console.log('Tus: ', Tus);
        const upload = new Tus.Upload(buffer, {
            // Endpoint is the upload creation URL from your tus server
            endpoint: `http://${backendHost}:1080/files/`,
            // Retry delays will enable tus-js-client to automatically retry on errors
            retryDelays: [0, 3000, 5000, 10000, 20000],
            // Callback for errors which cannot be fixed using retries
            onError: function (error) {
                console.log("Failed because: " + error)
            },
            // Callback for reporting upload progress
            onProgress: function (bytesUploaded, bytesTotal) {
                var percentage = (bytesUploaded / bytesTotal * 100).toFixed(2)
                console.log(bytesUploaded, bytesTotal, percentage + "%")
            },
            // Callback for once the upload is completed
            onSuccess: function () {
                console.log("Download %s from %s", upload.file.name, upload.url)
            }
        });

        console.log('upload: ', upload);
        upload.start();
    } finally {
        fs.unlinkSync(fileName);
        console.log('DONE!');
    }

}