# Run the entire application suite and keep it alive
run:
	docker-compose down --remove-orphans
	docker-compose build
	docker-compose config
	docker-compose up -d
	docker-compose logs -f

# Test all services that have tests
test:
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.test.yml \
		down --remove-orphans
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.test.yml \
		build
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.test.yml \
		config
	-docker volume rm upload_destination
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.test.yml \
		run --rm tus-client-test
	docker-compose \
		-f docker-compose.yml \
		-f docker-compose.test.yml \
		down --remove-orphans
	-docker volume rm upload_destination

# Connect to service.  Provide the name of the service to make
# with this syntax: `make connect service=tus-client`
connect:
	./script/knkt.sh $(service)